﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AvinodeXMLParse {
    class Program {
        static void Main(string[] args) {
            List<Menu> finalMenu = new List<Menu>();

            XmlFile file = Prompt();

            do {
                if (!File.Exists(file.FileLocation)) {
                    Console.Clear();
                    Console.WriteLine("File not found, enter new file path");
                    Console.WriteLine();
                    file = Prompt();
                }
            } while (!File.Exists(file.FileLocation));
            XmlDocument document = new XmlDocument();

            document.Load(file.FileLocation);


            foreach(XmlNode node in document.DocumentElement) {
                //RECURSION
                List<Menu> nodeValues = ProcessNode(node, file.DesiredPath);
                foreach(Menu menu in nodeValues) {
                    finalMenu.Add(menu);
                }
            }

            List<Menu> matches = ProcessMenu(finalMenu.Where(x => x.IsActive == true).ToList(), finalMenu);
            
            foreach(Menu match in matches) {
                foreach(Menu item in finalMenu.Where(x => x.MenuName == match.MenuName)) {
                    item.IsActive = true;
                }
            }

            Console.WriteLine();
            foreach(Menu item in finalMenu) {
                string leadingText = "";
                if(item.IsSubmenu) {
                     leadingText = AddTabs(item.Level);
                }
                Console.WriteLine(leadingText + item.MenuName + ", " + item.MenuPath + (item.IsActive ? " ACTIVE" : ""));
            }
            Console.ReadKey();
        }

        static XmlFile Prompt() {
            Console.WriteLine("Input File Location");
            string fileLocation = Console.ReadLine();
            Console.WriteLine("Input Desired Path");
            string path = Console.ReadLine();
            return new XmlFile() {
                FileLocation = fileLocation,
                DesiredPath = path
            };
        }

        static void ProcessFile() {

        }

        static List<Menu> ProcessNode(XmlNode node, string path, int level = 0, bool isSubMenu = false) {
            List<Menu> subMenuList = new List<Menu>();
            //RECURSION
            foreach (XmlNode child in node.ChildNodes) {
                string nodeName = "";
                string nodePath = "";
                string parentMenu = "";

                if (child.Name == "path") {
                    //Todo Find child
                    if (isSubMenu) {
                        parentMenu = child.ParentNode.ParentNode.ParentNode.FirstChild.InnerText;
                    } else {
                        parentMenu = child.ParentNode.FirstChild.InnerText;
                    }
                    nodeName = child.PreviousSibling.FirstChild.Value;
                    foreach(XmlAttribute attribute in child.Attributes) {
                        if(attribute.Name == "value") {
                            nodePath = attribute.Value;
                        }
                    }
                }

                if (nodeName != "" && nodePath != "") {
                    subMenuList.Add(new Menu {
                        MenuName = nodeName,
                        MenuPath = nodePath,
                        ParentMenu = parentMenu,
                        IsSubmenu = isSubMenu,
                        IsActive = (nodePath == path ? true : false),
                        Level = level
                    });
                }


                if (child.Name == "subMenu") {
                    level++;
                    foreach (XmlNode subMenu in child.ChildNodes) {
                            List<Menu> nodeValues = ProcessNode(subMenu, path, level, true);
                            foreach (Menu menu in nodeValues) {
                            subMenuList.Add(menu);
                            }
                        }
                    }
                }
            return subMenuList;
        }

        static List<Menu> ProcessMenu(List<Menu> items, List<Menu> fullMenu) {
            List<Menu> matches = new List<Menu>();
            foreach (Menu item in items) {
                List<Menu> parents = fullMenu.Where(x => x.MenuName == item.ParentMenu).ToList();
                foreach (Menu subItem in parents) {
                    matches.Add(subItem);
                    if (subItem.MenuName != subItem.ParentMenu) {
                        List<Menu> subMatches = ProcessMenu(parents, fullMenu);
                        foreach (Menu subMatch in subMatches) {
                            matches.Add(subMatch);
                        }
                    }
                }
            }
            return matches;
        }

        static string AddTabs(int level) {
            string retVal = "";
            for(int i = 0; i < level; i++) {
                retVal = retVal + "\t";
            }
            return retVal;
        }

        public class Menu {
            public string MenuName { get; set; }
            public string MenuPath { get; set; }
            public string ParentMenu { get; set; }
            public bool IsSubmenu { get; set; }
            public bool IsActive { get; set; }
            public int Level { get; set; }
        }

        public class XmlFile {
            public string FileLocation { get; set; }
            public string DesiredPath { get; set; }
        }
    }
}
